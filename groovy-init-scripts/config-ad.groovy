import jenkins.model.*
import hudson.model.*
import hudson.security.*
import hudson.plugins.*
import hudson.plugins.active_directory.*
import hudson.*
import jenkins.*

def instance = Jenkins.getInstance()

def env = System.getenv()

println "--> configure LDAP"

String domain = System.getenv("AD_NAME")
String site = 'site'
String server = System.getenv("AD_SERVER")
String bindName = System.getenv("AD_LOGIN")
String bindPassword = System.getenv("AD_PASSWORD")


adrealm = new ActiveDirectorySecurityRealm(domain, site, bindName, bindPassword, server)

adrealm.getDomains().each({
    it.bindName = adrealm.bindName
    it.bindPassword = adrealm.bindPassword
})
instance.setSecurityRealm(adrealm)


Jenkins.instance.save()

println "--> configure LDAP... done"

