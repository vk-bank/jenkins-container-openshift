FROM jenkins/jenkins:lts

ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false

RUN /usr/local/bin/install-plugins.sh active-directory role-strategy

COPY groovy-init-scripts/auth-config.groovy /usr/share/jenkins/ref/init.groovy.d/
COPY groovy-init-scripts/admin-user.groovy /usr/share/jenkins/ref/init.groovy.d/
COPY groovy-init-scripts/config-ad.groovy /usr/share/jenkins/ref/init.groovy.d/