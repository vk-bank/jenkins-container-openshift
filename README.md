# Jenkins in a container

## Overview

Jenkins in a container with active-directory plugin.

## Initial Setup on Amazon Linux

Install docker: `sudo yum install -y docker`

## Building the Docker Image

`docker build . -f Dockerfile -t vkhazin/jenkins:1.0`


## Publishing docker image

```bash
export DOCKER_ID_USER="docker hub username"
docker login -u $DOCKER_ID_USER
docker push $DOCKER_ID_USER/jenkins:1.0
```

## Deploying to OpenShift

### Create a new project

* Log-in using CLI: `./oc/oc login https://console.pro-us-east-1.openshift.com`
* Type password
* Create a new project using cli:
```
./oc/oc new-project jenkins-ad --description="Jenkins with AD, Running on OpenShift" --display-name="Jenkins AD project"
```
* Alternatively create a new project using web console.

### Create volumes

* Using web console select the newly created project
* Select `Storage` on the left hand navigation menu
* Select `Create Storage` with following options:
```
Storaje class: ebs
Name: vol1
Access mode: RWO
Size: 1GiB
```
* **Important:** Volume name `vol1` is used to map this volume inside container to /var/jenkins_home

### Deployment

* Run: 
```
./oc/oc create -f pod-config.yml \
  && ./oc/oc create -f service-config.yml \
  && ./oc/oc create -f route-config.yml
```

## Notes

* File [./pod_ad.json](./pod_ad.json) contains enviroment variables:
```
  - Java additional startup options:
    JAVA_OPTS="-Djenkins.install.runSetupWizard=false -Xms512M -Xmx512M"
```
* **Important:** incorrect memory limits will result in openshift klling Jenkins process
* Authentication parameters:
```
  - AD authentication parameters:
    AD_NAME="some.domain.name" 
    AD_SERVER="some.domain.name:389" (389 - port number, optional)
    AD_LOGIN='CN=loginname,CN=Group,DC=some,DC=domain,DC=name'
    AD_PASSWORD='somepassword'

  - Jenkins admin account (login from AD)
    JENKINS_USER="jenkins.user"

  - optional:
   HOME="/var/jenkins_home" \
```
* Current parameters have been pointed to a temporary Microsoft AD created for testing purposes only

### Removal

* Run: 
```
./oc/oc delete -f route-config.yml \
  && ./oc/oc delete -f service-config.yml \
  && ./oc/oc delete -f pod-config.yml
```
